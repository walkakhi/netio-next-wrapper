cmake_minimum_required(VERSION 3.17)

project(netio-next)

include(FetchContent)

FetchContent_Declare(netio
  GIT_REPOSITORY https://gitlab.cern.ch/walkakhi/netio-next.git
#  GIT_REPOSITORY https://gitlab.cern.ch/atlas-tdaq-felix/netio-next.git
  GIT_SUBMODULES ""
  GIT_SHALLOW YES
#  GIT_SUBMODULES_RECURSE ON
)
FetchContent_Populate(netio)
FetchContent_GetProperties(netio)

set(netio_src "${netio_SOURCE_DIR}/src")
message(STATUS "netio source dir: ${netio_SOURCE_DIR}")

FetchContent_Declare(felix-interface
  GIT_REPOSITORY https://gitlab.cern.ch/atlas-tdaq-felix/felix-interface.git
  GIT_SHALLOW YES
  GIT_SUBMODULES ""
)

FetchContent_Populate(felix-interface)
FetchContent_GetProperties(felix-interface)
message(STATUS "felix-interface source dir: ${felix-interface_SOURCE_DIR}")

set(if_src "${felix-interface_SOURCE_DIR}")

add_library(felix-interface INTERFACE)
target_include_directories(felix-interface INTERFACE ${if_src})




add_definitions(-DDEFAULT_DEBUG_LEVEL=3)
add_library(netio-next SHARED
  ${netio_src}/buffered.c
  ${netio_src}/completion_event.c
  ${netio_src}/completion_event.h
  ${netio_src}/completion_stack.c
  ${netio_src}/connection_event.c
  ${netio_src}/connection_event.h
  ${netio_src}/eventloop.c
  ${netio_src}/log.c
  ${netio_src}/log.h
  ${netio_src}/memory_registration.c
  ${netio_src}/netio.c
  ${netio_src}/netio_tcp.c
  ${netio_src}/pubsub.c
  ${netio_src}/semaphore.c
  ${netio_src}/socket_list.c
  ${netio_src}/subscription_list.c
  ${netio_src}/unbufpubsub.c
  ${netio_src}/util.c
  ${netio_src}/util.h
)




target_include_directories(netio-next PUBLIC ${netio_SOURCE_DIR}/netio)
target_include_directories(netio-next PUBLIC ${netio_SOURCE_DIR})
#target_include_directories(netio-next PRIVATE ${netio_SOURCE_DIR}/external/libfabric/1.16.1/x86_64-centos7-gcc11-opt)
#target_link_libraries(netio-next PRIVATE ${netio_SOURCE_DIR}/external/libfabric/1.16.1/x86_64-centos7-gcc11-opt/)

target_link_libraries(netio-next PUBLIC felix-interface)
target_link_libraries(netio-next PRIVATE fabric)


#include_directories(${netio_SOURCE_DIR}/external/libfabric/1.16.1/x86_64-centos7-gcc11-opt/include)

#add_library(libfabric SHARED IMPORTED GLOBAL) 
#set_target_properties(libfabric PROPERTIES IMPORTED_LOCATION ${netio_SOURCE_DIR}/external/libfabric/1.16.1/x86_64-centos7-gcc11-opt/lib/libfabric.so)
#target_link_libraries(netio-next libfabric pthread dl rt)


add_executable(link_test tests/link_text.cc)
target_link_libraries(link_test netio-next felix-interface)
#target_link_libraries(link_test netio-next libfabric)
#target_link_libraries(link_test netio-next felix-star)




